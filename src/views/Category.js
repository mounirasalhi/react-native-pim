import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Icon from "@expo/vector-icons/Ionicons";
import ItemList from "../components/ItemList";
import { ethers } from 'ethers';

import web3 from "web3";
//var Web3 = require('web3');

import '../../global';
const OpenEvents = require('../abis/OpenEvents.json');


//const web3 = new Web3('https://rinkeby.infura.io/v3/3d9be77ac95149e2bffe92864d0378ab');
import 'ethers/dist/shims.js';

//const currentProvider = new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/02f247358c4a43c3b8ef3f691c8ec61e');
//const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
const currentProvider = new web3.providers.HttpProvider('https://api.baobab.klaytn.net:8651');
//const provider = new ethers.providers.JsonRpcProvider('http://127.0.0.1:7545');
//const signer0 = provider.getSigner(0);
const provider = new ethers.providers.Web3Provider(currentProvider);
const contractAddress = "0xeF7D3cA332Ecd38F85921980c08b575C71032e63";
const contract = new ethers.Contract(contractAddress, OpenEvents.abi, provider);

const CATEGORY = [
  "BUY YOUR TICKET NOW",
 /*  "Handball",
  "Indiv Sport",
  "Skirts",
  "Dresses",
  "Shoes",
  "Shorts",
  "Skirts" */ 
];
const DRESSES1 = [
  {
    id: 1,
    imageUri: require("../../assets/dresses/dresses_1.jpg"),
    name: "EST-ESS",
    priceOne: 15,
    priceTwo: "20 Dt"
   }];

const DRESSES = [
  {
    id: 1,
    imageUri: require("../../assets/dresses/dresses_1.jpg"),
    name: "EST-ESS",
    priceOne: 15,
    priceTwo: "20 Dt"
   }
   //,
  // {
  //   id: 2,
  //   imageUri: require("../../assets/dresses/dresses_2.jpg"),
  //   name: "CSS-CAB",
  //   priceOne: 18,
  //   priceTwo: null
  // },
  // {
  //   id: 3,
  //   imageUri: require("../../assets/dresses/dresses_3.jpg"),
  //   name: "EST-CSS",
  //   priceOne: 14,
  //   priceTwo: null
  // },
  // {
  //   id: 4,
  //   imageUri: require("../../assets/dresses/dresses_4.jpg"),
  //   name: "CC-FDA",
  //   priceOne: 20,
  //   priceTwo: null
  // },
  // {
  //   id: 5,
  //   imageUri: require("../../assets/dresses/dresses_1.jpg"),
  //   name: "FGH-DFG",
  //   priceOne: 19,
  //   priceTwo: null
  // },
  // {
  //   id: 6,
  //   imageUri: require("../../assets/dresses/dresses_2.jpg"),
  //   name: "MPL-FGT",
  //   priceOne: 50,
  //   priceTwo: null
  // }
];

 /*
  const SHOES = [
  {
    id: 1,
    imageUri: require("../../assets/shoes/shoes_1.jpg"),
    name: "Helena",
    priceOne: 120,
    priceTwo: "$180"
  },
  {
    id: 2,
    imageUri: require("../../assets/shoes/shoes_2.jpg"),
    name: "Marie-Anne short",
    priceOne: 180,
    priceTwo: null
  },
  {
    id: 3,
    imageUri: require("../../assets/shoes/shoes_3.jpg"),
    name: "Betruschka",
    priceOne: 80,
    priceTwo: null
  },
  {
    id: 4,
    imageUri: require("../../assets/shoes/shoes_4.jpg"),
    name: "Betruschka",
    priceOne: 80,
    priceTwo: null
  },
  {
    id: 5,
    imageUri: require("../../assets/shoes/shoes_1.jpg"),
    name: "Betruschka",
    priceOne: 80,
    priceTwo: null
  },
  {
    id: 6,
    imageUri: require("../../assets/shoes/shoes_2.jpg"),
    name: "Betruschka",
    priceOne: 80,
    priceTwo: null
  }
];  */

class Category extends Component {


  constructor(props) {
    super();
this.state=({
        privateKey:'',
        address:'',
        ethbalance: '',
        transactionCount:'',
        block:'',
        name:'',
        date:'',
        price:null,
        seats:null,
        owner:null,
        name1:'',
        date1:'',
        price1:null,
        seats1:null,
        owner1:null,
        transaction:null,
        currentIndex: 0
    })
  };

async UNSAFE_componentWillMount  ()  {


let address = "0x22e54897D68A20aA04Cffca2519461D24712C3EE";

provider.getTransactionCount(address).then((transactionCount) => {
    console.log("Total Transactions Ever Sent: " + transactionCount);
});
// console.log(contract)
 //var currentValue = await contract.getEventsCount();
 var currentValue2 = await contract.getEvent(0);
console.log("getEvent :"+currentValue2.price);
//console.log("getEventsCount :"+currentValue);
 var getEventsCount = await contract.getEventsCount();
console.log("getEventsCount :"+getEventsCount);






//console.log(this.state.data.name)


//   data.setState({data:await contract.getEvent(0)})
   var currentValue2 = await  contract.getEvent(1);
   var currentValue = await  contract.getEvent(0);

this.setState({
           name:"Events :" +currentValue2.name,
           date:"Date : " + currentValue2.date,
           price:"Price :" + currentValue2.price,
           seats:"Seats :" +currentValue2.seats,
           owner:"Createur : " +currentValue2.owner
       });
       console.log(this.state.name)
       console.log(this.state.date)
       console.log(this.state.price)
       console.log(this.state.seats)
       console.log(this.state.owner)

this.setState({
           name1:"Events :" +currentValue.name,
           date1:"Date : " + currentValue.date,
           price1:"Price :" + currentValue.price,
           seats1:"Seats :" +currentValue.seats,
           owner1:"Createur : " +currentValue.owner
       });
       console.log(this.state.name1)
       console.log(this.state.date1)
       console.log(this.state.price1)
       console.log(this.state.seats1)
       console.log(this.state.owner1)


    }
  renderCategory = () => {
    return CATEGORY.map((item, i) => {
      return (
        <Text
          key={i}
          onPress={() => this.setState({ currentIndex: i })}
          style={{
            fontSize: 18,
            color: this.state.currentIndex === i ? "#F08C4F" : "white",
            paddingHorizontal: 10
          }}
        >
          {item}
        </Text>
      );
    });
  };

  renderItemList_Dress = () => {
    return DRESSES.map((item, i) => {
      return (
        <ItemList
          onPress={() =>
            this.props.navigation.navigate("Detail", {
              detailName: this.state.name,
              detailImageUri: item.imageUri,
              detailPriceOne: this.state.price,
              date: this.state.price,
              detailPriceTwo: item.priceTwo ? item.priceTwo : null
            })
          }
          key={item.id}
          imageUri={item.imageUri}
          name={this.state.name}
          priceOne={this.state.price}
          date= {this.state.price}
          priceTwo={item.priceTwo ? item.priceTwo : null}
        />
        
      );
    });
  };

  renderItemList_Shoes = () => {
    return DRESSES1.map((item, i) => {
      return (
        <ItemList
          onPress={() =>
            this.props.navigation.navigate("Detail", {
              detailName: item.name,
              detailImageUri: item.imageUri,
              detailPriceOne: item.priceOne,
              detailPriceTwo: item.priceTwo ? item.priceTwo : null
            })
          }
          key={item.id}
          imageUri={item.imageUri}
          name={item.name}
          priceOne={item.priceOne}
          priceTwo={item.priceTwo ? item.priceTwo : null}
        />
      );
    });
  };

  renderItemList = () => {
    if (this.state.currentIndex === 0) {
      return this.renderItemList_Dress();
    } else if (this.state.currentIndex === 1) {
      return this.renderItemList_Shoes();
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        {/* headerScrollHorizontal */}
        <View
          style={{
            height: hp("8%"),
            backgroundColor: "#63CBA7",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flex: 4
            }}
          >
            <ScrollView
              horizontal
              pagingEnabled
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                justifyContent: "center"
              }}
              ref={node => (this.scroll = node)}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center"
                }}
              >
                {this.renderCategory()}
              </View>
            </ScrollView>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Icon
              onPress={() => {
                this.scroll.scrollTo({ x: wp("80%") });
              }}
              name="ios-arrow-forward"
              size={20}
              color="white"
            />
          </View>
        </View>
        {/* headerScrollHorizontal */}

        {/* itemLists ScrollVertical */}
        <View
          style={{
            flex: 1
          }}
        >
          <ScrollView
            contentContainerStyle={{
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between"
            }}
          >
            {/* ItemList */}
            {this.renderItemList()}
          </ScrollView>
        </View>
        {/* itemLists ScrollVertical */}
      </View>
    );
  }
}

export default Category;
