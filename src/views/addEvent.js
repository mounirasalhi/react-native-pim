import React, { Component } from "react";
import {
  ImageBackground,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Image,View,ScrollView,TextInput

} from "react-native";
import DatePicker from 'react-native-datepicker'



import { Button, Block, Input, Text } from "../../component";
import { theme } from "../../constants";


import web3 from "web3";
//var Web3 = require('web3');

import '../../global';
const OpenEvents = require('../abis/OpenEvents.json');


//const web3 = new Web3('https://rinkeby.infura.io/v3/3d9be77ac95149e2bffe92864d0378ab');
import 'ethers/dist/shims.js';

//const currentProvider = new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/02f247358c4a43c3b8ef3f691c8ec61e');
//const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
const currentProvider = new web3.providers.HttpProvider('https://api.baobab.klaytn.net:8651');
//const provider = new ethers.providers.JsonRpcProvider('http://127.0.0.1:7545');
//const signer0 = provider.getSigner(0);
const provider = new ethers.providers.Web3Provider(currentProvider);
const contractAddress = "0xeF7D3cA332Ecd38F85921980c08b575C71032e63";
const contract = new ethers.Contract(contractAddress, OpenEvents.abi, provider);




// Import the ethers library
import { ethers } from 'ethers';
ethers.errors.setLogLevel("error");
let randomWallet = ethers.Wallet.createRandom();
//console.log(randomWallet);





export default class addEvent extends Component {
  constructor(props) {
    super();
this.state=({
        privateKey:'0xaa84042052ce0c1f08da4f51aad69482c349bbeb405ea205172a01197a7d5eb0',
        address:'',
        ethbalance: '',
        transaction:'',
        block:'',
        name:'',
        price:null,
        place:null,
        date:"2016-05-15"
    })
  };

async UNSAFE_componentWillMount () {
let address = "0x22e54897D68A20aA04Cffca2519461D24712C3EE";

provider.getTransactionCount(address).then((transactionCount) => {
    console.log("Total Transactions Ever Sent: " + transactionCount);
});


// console.log(contract)
// var currentValue = await contract.getEventsCount();
 //var currentValue2 = await contract.getEvent(0);
//console.log("getEvent :"+currentValue2);
//console.log("getEventsCount :"+currentValue);
let privateKey = '0xaa84042052ce0c1f08da4f51aad69482c349bbeb405ea205172a01197a7d5eb0';
let wallet = new ethers.Wallet(privateKey, provider);
console.log("Wallet address :"+wallet.address);
let contractWithSigner = contract.connect(wallet);
//var tx = await contractWithSigner.buyTicket(1,"0xe65e1f00e726137f77323b9c70a5894f6320377b");
//console.log(tx.hash);

//let t = await contractWithSigner.createEvent("React",7000000000000000,7,true,true,7,"string");

// See: https://ropsten.etherscan.io/tx/0xaf0068dcf728afa5accd02172867627da4e6f946dfb8174a7be31f01b11d5364
//console.log(t);

let history = await provider.getHistory("0x1515480db210A7Ec7FA60Bc82559198Ad06d5f98");
console.log(history);
provider.listAccounts().then(result => console.log(result))

}


 async addevent(name,price,place,date,privateKey){

var privateKey = privateKey;
var wallet = new ethers.Wallet(privateKey, provider);
console.log("Wallet address :"+wallet.address);
var contractWithSigner =  contract.connect(wallet);

var t = await contractWithSigner.
createEvent(name,
            date,
            price,
            true,
            true,
            place,
            "string");

  this.setState({transaction: t.hash})

console.log(t.hash)

console.log(date);
}
  
  handleLogin() {
    const { navigation } = this.props;
    const { address, privateKey } = this.state;
    const errors = [];

    
  }
 
  
    



  render() {
    const { navigation } = this.props;
   


    return (
              <ScrollView scrollEnabled>

      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block padding={[80, theme.sizes.base * 1]}>
          <View style={styles.container}>
          <Image 
    source={require('.../../assets/OIP.jpg')}  
       style={{width: 150, height: 150,alignItems:'center' }} />



</View>


          <Block middle>
            <Input
              label="Private Key"
              style={[styles.input]}
              multiline={true}
              numberOfLines={4}
              placeholder='0x6e53D7c173672EB18608102434c87024EeF5312F'
              defaultValue={this.state.privateKey}
              onChangeText={text => this.setState({ privateKey: text })}/>
             <Input
              label="Event Name"
              style={[styles.input]}
              placeholder='Mnjd'
              defaultValue={this.state.name}
              onChangeText={text => this.setState({ name: text })}/>
 <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date: date})}}
      />
      <View style={{flex: 1, flexDirection: 'row'}}>

 <View style={{width: 150, height: 150,padding :10}} >
 
             <Input
              label="Price"
              keyboardType={'numeric'}  
              style={[styles.input]}
              placeholder='1 ether'
              defaultValue={this.state.price}
              onChangeText={text => this.setState({ price: text })}/>
    </View>
     <View style={{width: 150, height: 150,padding:10}} >

             <Input
              label="Place"
              keyboardType={'numeric'}  
              style={[styles.input]}
              placeholder='12'
              defaultValue={this.state.place}
              onChangeText={text => this.setState({ place: text })}/>
      </View>
      </View>

                
                <Text
                       style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      > Transactions : {this.state.transaction}</Text>

                
                    
            
            <Button gradient onPress={()=>this.addevent(this.state.name,this.state.price,this.state.place,this.state.date,this.state.privateKey)}>

               
              
                <Text bold white center>
                  Get Balance
                </Text>
              
            </Button>

               <Button onPress={() => navigation.navigate("Ethereum_wallet")}>
              <Text
                gray
                caption
                center
                style={{ textDecorationLine: "underline" }}
              >
                Back to Ethereum
              </Text>
            </Button>
          
          
          </Block>
        </Block>
      </KeyboardAvoidingView>
        </ScrollView>

    );

   
   

   
}
}
const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    paddingBottom: 10,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  },
  container:{
    flex: 1,
    alignItems :'center',
    justifyContent :'center'
  }
});
