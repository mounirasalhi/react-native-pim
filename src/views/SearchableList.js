import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator,Image } from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements';
import { ethers } from 'ethers';

import web3 from "web3";
//var Web3 = require('web3');

import '../../global';
const OpenEvents = require('../abis/OpenEvents.json');


//const web3 = new Web3('https://rinkeby.infura.io/v3/3d9be77ac95149e2bffe92864d0378ab');
import 'ethers/dist/shims.js';

//const currentProvider = new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/02f247358c4a43c3b8ef3f691c8ec61e');
//const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
const currentProvider = new web3.providers.HttpProvider('https://api.baobab.klaytn.net:8651');
//const provider = new ethers.providers.JsonRpcProvider('http://127.0.0.1:7545');
//const signer0 = provider.getSigner(0);
const provider = new ethers.providers.Web3Provider(currentProvider);
const contractAddress = "0xeF7D3cA332Ecd38F85921980c08b575C71032e63";
const contract = new ethers.Contract(contractAddress, OpenEvents.abi, provider);
const numbers = [];
class FlatListDemo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

 async makeRemoteRequest  ()  {
    const url = `https://randomuser.me/api/?&results=20`;
    this.setState({ loading: true });
let address = "0x22e54897D68A20aA04Cffca2519461D24712C3EE";

provider.getTransactionCount(address).then((transactionCount) => {
    console.log("Total Transactions Ever Sent: " + transactionCount);
});
// console.log(contract)
 //var currentValue = await contract.getEventsCount();
 var currentValue2 = await contract.getEvent(0);
console.log("getEvent :"+currentValue2.price);
//console.log("getEventsCount :"+currentValue);
 var getEventsCount = await contract.getEventsCount();
console.log("getEventsCount :"+getEventsCount);




this.setState({
           data: currentValue2,
         //  error: Result.error || null,
           loading: false,
       });
//console.log(this.state.data.name)

         for (var i=0; i < getEventsCount; i++) {
//   data.setState({data:await contract.getEvent(0)})
   var currentValue2 = await  contract.getEvent(i);

    console.log(currentValue2.owner);

    }
  
  
    // fetch(url)
    //   .then(res => res.json())
    //   .then(res => {
    //     this.setState({
    //       data: res.results,
    //       error: res.error || null,
    //       loading: false,
    //     });
    //     this.arrayholder = res.results;
    //   })
    //   .catch(error => {
    //     this.setState({ error, loading: false });
    //   });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.name.toUpperCase()} ${item.name.toUpperCase()} ${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Type Here..."
        lightTheme
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
        value={this.state.value}
      />
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        
        
          
           <Text >{this.state.data}</Text>
      </View>
    );
  }

async UNSAFE_componentWillMount () {
let address = "0x22e54897D68A20aA04Cffca2519461D24712C3EE";

provider.getTransactionCount(address).then((transactionCount) => {
    console.log("Total Transactions Ever Sent: " + transactionCount);
});
// console.log(contract)
 //var currentValue = await contract.getEventsCount();
 var currentValue2 = await contract.getEvent(0);
console.log("getEvent :"+currentValue2.price);
//console.log("getEventsCount :"+currentValue);
}
}
export default FlatListDemo;
