import React, { Component } from "react";
import {
  ImageBackground,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Image,View,ScrollView

} from "react-native";


import { Button, Block, Input, Text } from "../../component";
import { theme } from "../../constants";


import web3 from "web3";
//var Web3 = require('web3');

import '../../global';
const OpenEvents = require('../abis/OpenEvents.json');


//const web3 = new Web3('https://rinkeby.infura.io/v3/3d9be77ac95149e2bffe92864d0378ab');
import 'ethers/dist/shims.js';

//const currentProvider = new web3.providers.HttpProvider('https://rinkeby.infura.io/v3/02f247358c4a43c3b8ef3f691c8ec61e');
//const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
const currentProvider = new web3.providers.HttpProvider('https://api.baobab.klaytn.net:8651');
//const provider = new ethers.providers.JsonRpcProvider('http://127.0.0.1:7545');
//const signer0 = provider.getSigner(0);
const provider = new ethers.providers.Web3Provider(currentProvider);
const contractAddress = "0xeF7D3cA332Ecd38F85921980c08b575C71032e63";
const contract = new ethers.Contract(contractAddress, OpenEvents.abi, provider);





// Import the ethers library
import { ethers } from 'ethers';
ethers.errors.setLogLevel("error");
let randomWallet = ethers.Wallet.createRandom();
//console.log(randomWallet);





export default class Ethereum_wallet extends Component {
  constructor(props) {
    super();
this.state=({
        privateKey:'',
        address:'',
        ethbalance: '',
        transactionCount:'',
        block:'',
        eventName:'',
        price:null,


    })
  };




async getBalance(address){
  this.setState({ethbalance: ''})

provider.getBlockNumber().then((blockNumber) => {
    console.log("Current block number: " + blockNumber);
        this.setState({block: blockNumber})

});

provider.getBalance(address).then((balance) => {

    // balance is a BigNumber (in wei); format is as a sting (in ether)
    let etherString = ethers.utils.formatEther(balance);
this.state.ethbalance=etherString;
    console.log("Balance: " + etherString );
  this.setState({ethbalance: etherString})


var etherscanProvider = new ethers.providers.EtherscanProvider();

});
//currentProvider.eth.getAccounts(console.log);
var ticketsOf = await contract.getTicket(0);
console.log(ticketsOf.eventName)
console.log(ticketsOf.owner)
console.log("Price :"+ ticketsOf.price)

this.setState({
           eventName:"Events :" +ticketsOf.eventName,
           price:"Price :" + ticketsOf.price +"Ether",
       });
provider.getTransactionCount(address).then((transactionC) => {
    console.log("Total Transactions Ever Sent: " + transactionC);
    this.setState({transactionCount: transactionC})
});  





/* currentProvider.getHistory(address).then((history) => {
    history.forEach((tx) => {
        console.log(tx);
    })
}) */
}
  
  handleLogin() {
    const { navigation } = this.props;
    const { address, privateKey } = this.state;
    const errors = [];

    
  }
  
async UNSAFE_componentWillMount () {
let address = "0x22e54897D68A20aA04Cffca2519461D24712C3EE";

provider.getTransactionCount(address).then((transactionCount) => {
    console.log("Total Transactions Ever Sent: " + transactionCount);
});


// console.log(contract)
// var currentValue = await contract.getEventsCount();
 //var currentValue2 = await contract.getEvent(0);
//console.log("getEvent :"+currentValue2);
//console.log("getEventsCount :"+currentValue);
let privateKey = '0xaa84042052ce0c1f08da4f51aad69482c349bbeb405ea205172a01197a7d5eb0';
let wallet = new ethers.Wallet(privateKey, provider);
console.log("Wallet address :"+wallet.address);
let contractWithSigner = contract.connect(wallet);
//var tx = await contractWithSigner.buyTicket(1,"0xe65e1f00e726137f77323b9c70a5894f6320377b");
//console.log(tx.hash);

//let t = await contractWithSigner.createEvent("React",7000000000000000,7,true,true,7,"string");

// See: https://ropsten.etherscan.io/tx/0xaf0068dcf728afa5accd02172867627da4e6f946dfb8174a7be31f01b11d5364
//console.log(t);

let history = await provider.getHistory("0x1515480db210A7Ec7FA60Bc82559198Ad06d5f98");
console.log(history);
provider.listAccounts().then(result => console.log(result))


}
  
  
    



  render() {
    const { navigation } = this.props;
   


    return (
              <ScrollView scrollEnabled>

      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block padding={[80, theme.sizes.base * 1]}>
          <View style={styles.container}>
          <Image 
    source={require('.../../assets/OIP.jpg')}  
       style={{width: 150, height: 150,alignItems:'center' }} />



</View>
<Text

style={{ fontWeight: 'bold',textAlign:'left',padding:5 }}
 h2>Address 
 </Text> 
          <Block middle>
            <Input
              label="Address"
              style={[styles.input]}
              placeholder='0x6e53D7c173672EB18608102434c87024EeF5312F'
              defaultValue={this.state.address}
              onChangeText={text => this.setState({ address: text })}/>
            
                <Text
                      style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      >Wallet Balance: {this.state.ethbalance}</Text>
                      <Text
                       style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      >Total Transactions Ever Sent: {this.state.transactionCount}</Text>

                 <Text
                       style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      >Current block number: {this.state.block}</Text>     
            <Text

style={{ fontWeight: 'bold',textAlign:'left',padding:5 }}
 h2>Ticket Buy
 </Text> 
<Text
                      style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      > {this.state.eventName}</Text>
                      <Text
                      style={{
                    color: "rgba(9, 188, 157, 0.9)"
                  }}
                            onValueChange={() => this._onPressButton()}

                      > {this.state.price}</Text>
            <Button gradient onPress={()=>this.getBalance(this.state.address)}>

               
              
                <Text bold white center>
                  Get Balance
                </Text>
              
            </Button>

               <Button onPress={() => navigation.navigate("addEvent")}>
              <Text
                gray
                caption
                center
                style={{ textDecorationLine: "underline" }}
              >
                Add Event
              </Text>
            </Button>


          </Block>
        </Block>
      </KeyboardAvoidingView>
        </ScrollView>

    );

    <View style={styles.container}>
        <Text>Latest ethereum block is: {latestBlockNumber}</Text>
        <Text>Check your console!</Text>
        <Text>You should find extra info on the latest ethereum block.</Text>
      </View>
  }

   
}

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    paddingBottom: 10,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  },
  container:{
    flex: 1,
    alignItems :'center',
    justifyContent :'center'
  }
});
