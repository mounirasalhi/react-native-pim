import * as React from 'react';
import * as Facebook from 'expo-facebook';
import { Ionicons } from '@expo/vector-icons'

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input  } from 'react-native-elements';
import * as AppAuth from 'expo-app-auth';

import '../../global';


import {
  
  Image,
  Alert,
  ActivityIndicator,ScrollView,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableOpacity,
  ImagePickerIOS,View,

} from "react-native";

import { Button, Block, Text } from "../../component";
import { theme } from "../../constants";




import firebase from '../database/firebase';


export default class Register extends React.Component {

    constructor() {
    super();
    this.state = { 
      displayName: '',
      email: '', 
      password: '',
      isLoading: false
    }
  }   



handlePickAvatar = async () => {
  UserPermissions.getCameraPermission()
  let result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes:ImagePicker.MediaTypeOptions.Images,
    allowEditing:true,
    aspect: [4,3]
  });
   if (!result.cancelled){
    this.setState({...this.state, avatar: result.uri})
 }
 
};

 
componentWillUnmount () {
    this.getPermissionAsync();

  firebase.auth().onAuthStateChanged((user) => {
    if (user != null) {
      console.log(user)
    }
  })
} 
  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signup!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        res.user.updateProfile({
          displayName: this.state.displayName
        })
        console.log('User registered successfully!')
        this.setState({
          isLoading: false,
          displayName: '',
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Home')
      })
      .catch(error => this.setState({ errorMessage: error.message }))      
    }
  }

async  onFacebookButtonPress() {
  const { navigation } = this.props;
  
  try {
    await Facebook.initializeAsync('185112815895409');
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions,
    } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ['public_profile','email'],
    });
    if (type === 'success') {
      // Get the user's name using Facebook's Graph API
     //const response = await fetch(
       // `https://graph.facebook.com/me?access_token=${token}&fields=id,name,birthday,picture.type(large)`
      //);      
            const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture`);
          ///Alert.alert('Logged in!', `Hi ${(await response.json()).id}!`);

    //console.log(await response.json()).name;
let name = (await response.json()).displayName
console.log(name)
    this.setState({
      displayName: name
    })

 const credential = firebase.auth.FacebookAuthProvider.credential(token)
firebase.auth().signInWithCredential(credential).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // The email of the user's account used.
  var email = error.email;
  
  // The firebase.auth.AuthCredential type that was used.
  var credential = error.credential;
  if (errorCode === 'auth/account-exists-with-different-credential') {
    alert('Email already associated with another account.');
    // Handle account linking here, if using.
  } else {
    console.error(error);
  
  
  }
 });

            navigation.navigate('Home');

    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
    
    

  }

  handleSignUp() {
    const { navigation } = this.props;
  }


    
getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };


 _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      
      
      if (!result.cancelled) {
      this.setState({ image: result.uri });
      this.uploadImage(result.uri, "test-image")
        .then(() => {
          Alert.alert("Success");
          
        })
        .catch((error) => {
          Alert.alert(error.message);
          console.console.log(error.message);
          
        });
    }
       

      console.log(result);
    } catch (E) {
      console.log(E);
    }
  };

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();

    var ref = firebase.storage().ref().child("images/" + imageName);
    return ref.put(blob);
  }

  render() {
    const { navigation } = this.props;

 if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
      }    
    let { image } = this.state;

    return (

              <ScrollView scrollEnabled>

      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block
         padding={[50, theme.sizes.base * 1]}>
          <View style={{  alignItems: 'center' }}>

 <Button  color="#ffffff" onPress={this._pickImage}>
        <Text   bold color="#2f4f4f" center>
Uploade your image
          </Text>
        </Button>

        {image && <Image source={{ uri: image }} style={{ width: 100, height: 100,borderRadius: 400/ 2 }} />}
</View>
          <Block middle>
        
        
           <Input
              email
              placeholder='med@gmail.com'
              label="Email"
              style={[styles.input]}
              value={this.state.email}
              onChangeText={(val) => this.updateInputVal(val, 'email')}

              leftIcon={
    <Icon
      name='envelope'
      size={15}
      color='#87CEFA'
    />
  }
            />
            <Input
              label="Username"
              placeholder='Username'
              style={[styles.input]}
              value={this.state.displayName}
              onChangeText={(val) => this.updateInputVal(val, 'displayName')}
              leftIcon={
    <Icon
      name='user'
      size={15}
      color='#87CEFA'
    />
  }

            />
            <Input
              placeholder='password'

              secureTextEntry={true}
              label="Password"
              leftIcon={
    <Icon
      name='lock'
      size={20}
      color='#87CEFA'
    />
  }       
          style={[styles.input]}
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
            />
                     
           <Button gradient
             onPress={()=>this.registerUser()}
             >
           
                <Text bold white center>
                  Sign Up
                </Text>
              
            </Button>
            
            <Button color='#1e90ff' 
      onPress={() => this.onFacebookButtonPress().then(() => console.log('Signed in with Facebook!'))}
            >
              <Text   bold white center>
                  Sign Up with Facebook
              </Text>
           </Button>   
            



               <Button onPress={() => navigation.navigate("Login")
}>
              <Text 
                gray
                caption
                center

              >
                Back to Login
              </Text>
            </Button> 


          </Block>
        </Block>
      </KeyboardAvoidingView>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
pos:{
  position:"absolute",
    bottom: -325,
    right:-225,
},
  avatar: {
    position:"absolute",
    width:70,
    height: 70,
    borderRadius:50
  },
  avatarPlaceholder: {
    left :120,
    width:70,
    height: 70,
    borderRadius:50,
    backgroundColor: "#E1E2E6",
    marginTop:48,
    justifyContent:"center",
    alignItems:"center"
    

  },
  signup: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  },
  MyButton:{
    padding: 5,
    height: 100,
    width: 100,  //The Width must be the same as the height
    borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
    //backgroundColor:'rgb(195, 125, 198)',
  }

});
