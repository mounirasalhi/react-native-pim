import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import HomeCategory from "../components/HomeCategory";
import HomeCategory1 from "../components/HomeCategory1";

class Home extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <ScrollView scrollEnabled>
          <HomeCategory
            imageUri={require("../../assets/women_fashion.png")}
            titleFirst="SPORT "
            titleSecond="EVENTS"
            subTitle="Matchs , Competitions , indiv.Sports ..."
            screenProps="Super"
            {...this.props}
          />
          <HomeCategory
          
            imageUri={require("../../assets/men_fashion.jpg")}
            titleFirst="CINEMA"
            titleSecond="& THEATER"
            subTitle="Exclusive Films , Scenes .."
            {...this.props}
          />
          <HomeCategory
            imageUri={require("../../assets/kids_fashion.png")}
            titleFirst="Shows"
            titleSecond="Summer "
            subTitle="Festivals , Signing Show , Stages .."
            {...this.props}
          />
        </ScrollView>
      </View>
    );
  }
}

export default Home;
