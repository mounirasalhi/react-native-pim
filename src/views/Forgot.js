import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,ScrollView,View,Image
} from "react-native";

import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';

import { Button, Block, Text } from "../../component";
import { theme } from "../../constants";
import firebase from '../database/firebase';

const VALID_EMAIL = "mounira.salhi@esprit.tn";


export default class Forgot extends Component {
  
  constructor (props){
    super(props)
    this.state=({
        email:'',
        password:''
            })
}
componentDidUpdate () {

  firebase.auth().onAuthStateChanged((user) => {
    if (user != null) {
      console.log(user)
    }
  })
}
forgotPassword = (email) => {
  firebase.auth().sendPasswordResetEmail(email)
    .then(function (user) {
      alert('Please check your email...')
    }).catch(function (e) {
      console.log(e)
    })
}


  /*handleForgot() {
    const { navigation } = this.props;
    const { email } = this.state;
    const errors = [];

    Keyboard.dismiss();
    this.setState({ loading: true });

    // check with backend API or with some static data
    if (email !== VALID_EMAIL) {
      errors.push("email");
    }

    this.setState({ errors, loading: false });

    if (!errors.length) {
      Alert.alert(
        "Password sent!",
        "Please check you email.",
        [
          {
            text: "OK",
            onPress: () => {
              navigation.navigate("Login");
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      Alert.alert(
        "Error",
        "Please check you Email address.",
        [{ text: "Try again" }],
        { cancelable: false }
      );
    }
  }
*/
  render() {
    const { navigation } = this.props;
    const { loading, errors } = this.state;
    const hasErrors = key => (errors.includes(key) ? styles.hasErrors : null);

    return (
      
              <ScrollView scrollEnabled>

      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block
         padding={[50, theme.sizes.base * 1]}>
          <View style={styles.container}>
          <Image 
    source={require('.../../assets/splash.png')}  
       style={{width: 350, height: 250,alignItems:'center' }} />
<Button >
              <Text
                bold
                caption
                center
              >
Recover your password: by clicking "Forgot" and check your inbox
              </Text>
            </Button>

</View>
          <Block middle>
            <Input
             label="Email"
             leftIcon={
             <Icon
             name='envelope'
             size={15}
             color='#87CEFA'
             />
              }
            style={[styles.input]}
            placeholder='med@gmail.com'
            defaultValue={this.state.email}
            onChangeText={text => this.setState({ email: text })}             
             />
             <Button gradient onPress={() => this.forgotPassword(this.state.email)
              //this.handleForgot()
            }>
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text bold white center>
                  Forgot
                </Text>
              )}
            </Button>

            <Button onPress={() => navigation.navigate("Login")}>
              <Text
                gray
                caption
                center
                style={{ textDecorationLine: "underline" }}
              >
                Back to Login
              </Text>
            </Button>
 
  
                     
          
           
           
            



           
          </Block>
        </Block>
      </KeyboardAvoidingView>
        </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  forgot: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  }
});
