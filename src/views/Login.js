import React, { Component } from "react";
import {
  ImageBackground,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,View,Image,ScrollView,Alert
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';

import { Button, Block, Text } from "../../component";
import { theme } from "../../constants";
import firebase from '../database/firebase';




import '../../global';


export default class Login extends Component {
  
  constructor() {
    
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false
    }
  }

UNSAFE_componentWillMount () {
}
  
  
  
   updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
      const { navigation } = this.props;
      

    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res)
        console.log('User logged-in successfully!')
        this.setState({
          isLoading: false,
          email: '', 
          password: ''
        })
            navigation.navigate('Home');
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  handleLogin() {
    const { navigation } = this.props;
    const { email, password } = this.state;
    const errors = [];

    
  }

  render() {
    const { navigation } = this.props;
   


    
    return (
              <ScrollView scrollEnabled>

      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block
         padding={[50, theme.sizes.base * 1]}>
          <View style={styles.container}>
          <Image 
    source={require('.../../assets/splash.png')}  
       style={{width: 350, height: 250,alignItems:'center' }} />


</View>
          <Block middle>
            <Input
             label="Email"
leftIcon={
    <Icon
      name='envelope'
      size={15}
      color='#87CEFA'
    />
  }            
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
             style={[styles.input]}

              placeholder='med@gmail.com'
              onChangeText={text => this.setState({ email: text })}
              />
           <Input
              secureTextEntry={true}
              label="Password"
              leftIcon={
    <Icon
      name='lock'
      size={20}
      color='#87CEFA'
    />
  }              style={[styles.input]}
    value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
              placeholder='*******'
              style={[styles.input]}

              onChangeText={text => this.setState({ password: text })}
            /> 
                     
            <Button gradient onPress={()=>this.userLogin()
            }>
              
               
              
                <Text bold white center>
                  Login
                </Text>
              
            </Button>     
            


<Button onPress={() => navigation.navigate("Forgot")}>
              <Text
                gray
                caption
                center

              >
                Forgot your password?
              </Text>
            </Button>
            <Button onPress={() => navigation.navigate("Register")}>
              <Text
                bold
                caption
                center
                
              >
                Not a member yet ? Register now
              </Text>
            </Button>
          </Block>
        </Block>
      </KeyboardAvoidingView>
        </ScrollView>

    );



  }

   
}

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    paddingBottom: 10,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  },
    container:{
    flex: 1,
    alignItems :'center',
    justifyContent :'center'
  }
});
