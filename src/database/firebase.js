import * as firebase from 'firebase';


const firebaseConfig  = {
  apiKey: "AIzaSyDfgdXfU-PmG3xhzy60tN0Ybtxnr_95xBk",
  authDomain: "pim-react-native.firebaseapp.com",
  databaseURL: "https://pim-react-native.firebaseio.com",
  projectId: "pim-react-native",
  storageBucket: "pim-react-native.appspot.com",
  messagingSenderId: "400133452288",
  appId: "1:400133452288:web:a19c707687b66b67efdee5",
  measurementId: "G-1Q18HM55SZ"
};

firebase.initializeApp(firebaseConfig);
if (!firebase.apps.length) {
   firebase.initializeApp({});
}

export default firebase;