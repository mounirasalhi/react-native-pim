import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import firebase from '../database/firebase';

  const user = firebase.auth().currentUser;



class CustomDrawerComponent extends Component {
    constructor() {
    super();
    this.state = { 
      uid: '',
      name:'',
      displayName:'',
      picture:''
    }
//        if(user.photoURL === 'null'){
// this.setState({
//       picture: 'require("../../assets/reviewer.jpg")'
//     })
    
//    }
    }
    
    
    
  

 UNSAFE_componentWillMount () {
var name, email, photoUrl, uid, emailVerified;

if (user != null) {
  
  name = user.displayName;
  console.log(name)
  email = user.email;
  //photoUrl = user.photoURL;
  console.log(user.photoURL)

  emailVerified = user.emailVerified;
  uid = user.uid;  // The user's ID, unique to the Firebase project. Do NOT use
                   // this value to authenticate with your backend server, if


}
                   // you have one. Use User.getToken() instead.
   
   


     this.setState({
      name: name
    })
     this.setState({
      uid: uid
    })
    //this.setState({
      //picture: photoUrl
   // })

} 
 signOut = () => {

      this.props.navigation.navigate('Login')
    this.state = { 
      uid: '',
      name:'',
      displayName:''
    }
    

  }  



  render() {

 this.state = { 
  

      displayName: this.state.name,
      uid: this.state.uid,
     // picture: this.state.picture
    }


    return (
      <View
        style={{
          flex: 1
        }}
      >
      
        <ImageBackground
          source={require("../../assets/images/illustration_11.png")}
          style={{
            flex: 1,
            width: "100%",
            height: "80%",
            resizeMode: "repeat"
          }}
        >
          <View
            style={{
              flex: 1,
              //backgroundColor: "rgba(9, 188, 157, 0.9)",
              paddingTop: wp("20%"),
              paddingHorizontal: wp("8%"),
              paddingBottom: wp("12%")
            }}
          >
            <View
              style={{
                flexDirection: "column",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              
              
              
            </View>
            <View
              style={{
                flex: 2,
                justifyContent: "space-between",
                marginVertical: 20,
                padding: 1
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("Home");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >
                <Ionicons name="ios-home" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontStyle:'italic',
                    padding :5,
                    fontSize: 25,
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                >
                  Home
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("Home");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >
                <Ionicons name="ios-search" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontStyle:'italic',
                    fontSize: 25,
                    padding :5,
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                >
                  Search
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("Home");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >
                <Ionicons name="ios-list-box" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontSize: 25,
                   fontStyle:'italic',
                    fontWeight: "400",
                    padding :5,
                    marginLeft: wp("4.5%")
                  }}
                >
                  Categories
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("Basket");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >



                <Ionicons name="ios-cart" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontSize: 25,
                    fontStyle:'italic',
                    padding :5,
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                >
                  Basket
                </Text>
              </TouchableOpacity>
  <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("Ethereum_wallet");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >

                <Icon name="ethereum" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontSize: 25,
                    padding :5,
                    fontStyle:'italic',
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                >
                  Etheureum Wallet
                </Text>
              </TouchableOpacity>
  <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
                  this.props.navigation.navigate("FlatListBasics");
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >

                <Icon name="ethereum" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    fontSize: 25,
                    padding :5,
                    fontStyle:'italic',
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                >
                  Events
                </Text>
              </TouchableOpacity>



  <TouchableOpacity
                onPress={() => {
                  this.props.navigation.closeDrawer();
           this.signOut();
                            

                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >

                <Icon name="logout" color="black" size={25} />
                <Text
                  style={{
                    color: "black",
                    padding :5,
                    fontSize: 25,
                    fontStyle:'italic',
                    fontWeight: "400",
                    marginLeft: wp("4.5%")
                  }}
                  
                >
Logout
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "flex-end"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                {/* <View
                  style={{
                    width: wp("20%"),
                    height: wp("20%"),
                    overflow: "hidden",
                    borderRadius: wp("10%"),
                    marginRight: wp("4.5%")
                  }}
                >
                  <Image
            //source={require("../../assets/reviewer.jpg")}
                    
            source={{ uri: firebase.auth().currentUser.photoURL }}
            defaultSource={require("../../assets/reviewer.jpg")}

                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: "contain"
                    }}
                  />
                </View> */}
                <TouchableOpacity
                onPress={() => {
                  
                  //this.props.navigation.navigate("Settings");
                }}
                style={{
                  flexDirection: "column",
                  alignItems: "center",
                  paddingVertical: 5
                }}
              >
                
                <Text
                  style={{
                    color: "black",
                    fontSize: 25,
                    fontWeight: "400",
                    fontStyle:'italic',
                  }}
                >
                 
                  {firebase.auth().currentUser.displayName} 
                </Text>
                  <Text
                  style={{
                    color: "black",
                    fontSize: 10,
                    fontWeight: "400",
                    fontStyle:'italic',
                  }}
                >
                 
                  {firebase.auth().currentUser.email} 
                </Text>


              </TouchableOpacity>
              
               {/*  <Text 
                  style={{
                    color: "white",
                    fontSize: 25,
                    fontWeight: "400"
                  }}
                >
                  Account
                </Text> */}
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default CustomDrawerComponent;

{
  /* <Text> CustomDrawerComponent </Text>
        <Button
          title="go to Basket"
          onPress={() => this.props.navigation.navigate("Basket")}
        /> */
}
