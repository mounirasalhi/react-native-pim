const HDWalletProvider = require("@truffle/hdwallet-provider");
var config = require("./config.json");
var privateKeys = [
  "B26A8DFB6028833934D0FA768A078A3B1E528BE719B2A526DEB6DA48FC772902",
  "3AEBCAD1C1418B021D0D4E5F7B53319B6B855904CBB76EAF1E4112E854EE2E5C",
  
];

module.exports = {
	
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
		compilers: {
		  solc: {
			version: "0.4.24",
		  },
		},
		
	  
	
	networks: {
		
		rinkeby: {
			// provider: function () {
			// 	return new HDWalletProvider(config.wallet, "https://rinkeby.infura.io/v3/"+config.infura)
			// },
	        provider : ( )  =>  new  HDWalletProvider ( privateKeys,
			 "https://rinkeby.infura.io/v3/"+config.infura) ,

			network_id: 4,
			gas:7000000,
			gasPrice: 10000000000,



			solc: {
				optimizer: {
					enabled: true,
					runs: 200
				}
			}
		}
		,
		ganache: {
			 host: "localhost",
			 port: 7545,
			 network_id: "5777"
			// host: "localhost",
            // port: 8545,
            // network_id: "*" // Match any network id
		}
	}
};

/* Gas est une unité qui mesure la quantité 
d'effort de calcul qu'il faudra pour exécuter certaines opérations
Celle-ci sert à payer les frais de transaction au sein du réseau.
 */

/*  Ethereum dispose de trois réseaux de test 
pour que le développeur puisse développer et
 faire des tests, à savoir ROPSTEN, KOVAN et RINKEBY.
 */