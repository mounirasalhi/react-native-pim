import React from "react";
import { Dimensions } from "react-native"
 import {
  
  createSwitchNavigator,
  createAppContainer
  
} from "react-navigation"; 
import { createDrawerNavigator } from 'react-navigation-drawer'; 
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator,createBottomTabNavigator } from 'react-navigation-stack';
import Icon from "@expo/vector-icons/Ionicons";
import Login from "./src/views/Login";
import Register from "./src/views/Register";
import Home from "./src/views/Home";
import FlatListBasics from "./src/views/FlatListBasics";


import Ethereum_wallet from "./src/views/Ethereum_wallet";
import Category from "./src/views/Category";
import Detail from "./src/views/Detail";
import Basket from "./src/views/Basket";
import EditBasket from "./src/views/EditBasket";
import Address from "./src/views/Address";
import Shipping from "./src/views/Shipping";
import Payment from "./src/views/Payment";
import TermsAndConditions from "./src/views/TermsAndConditions";
import CreditCard from "./src/views/CreditCard";
import CustomDrawerComponent from "./src/components/CustomDrawerComponent";
import welcome from "./src/views/Welcome";
import Forgot from "./src/views/Forgot";
import Settings from "./src/views/Settings";
import CategoryCin from "./src/views/CategoryCin";
import addEvent from "./src/views/addEvent";

export default class App extends React.Component {

  render() {
    return <AppContainer />;
  }
}

const paymentStackNavigator = createStackNavigator(
  {
    Payment: {
      screen: Payment
    },
    CreditCard: {
      screen: CreditCard
    }
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

const CheckoutTabNavigator = createMaterialTopTabNavigator(
  {
    Address: {
      screen: Address
    },
    Shipping: {
      screen: Shipping
    },
    Payment: {
      screen: paymentStackNavigator
    }
  },
  {
    swipeEnabled: false,
    tabBarOptions: {
      activeTintColor: "#F08C4F",
      style: {
        backgroundColor: "#63CBA7"
      },
      indicatorStyle: {
        backgroundColor: "#F08C4F"
      }
    }
  }
);

const HomeStackNavigator = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Home",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
         headerLeft: (
           <Icon
             onPress={() => navigation.openDrawer()}
             name="md-menu"
             color="black"
             size={30}
             style={{
               paddingLeft: 10
             }}
           />
         ),
        headerRight: (
          <Icon
            onPress={() => navigation.openDrawer()}
            name="ios-search"
            color="white"
            size={30}
            style={{
              paddingRight: 10
            }}
          />
        )
      };
    }
  },
  Category: {
    screen: Category,CategoryCin,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: navigation.state.params.name,
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     color="white"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // ),
        headerRight: (
          <Icon
            onPress={() => navigation.navigate("Basket")}
            name="md-cart"
            color="white"
            size={30}
            style={{
              paddingRight: 10
            }}
          />
        )
      };
    }

    
  },
  
  Basket: {
    screen: Basket,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Basket",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // )
      };
    }
  },
  
  Basket: {
    screen: Basket,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Basket",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // )
      };
    }
  },
  
  Ethereum_wallet: {
    screen: Ethereum_wallet,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Ethereum wallet",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
       // headerShown: false,

        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // )
      };
    }
  },
    FlatListBasics: {
    screen: FlatListBasics,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Events",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
       // headerShown: false,

        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // )
      };
    }
  },
  addEvent: {
    screen: addEvent,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: "Add Event",
        headerTitleStyle: {
          color: "black"
        },
        headerStyle: {
          backgroundColor: "#ffffff"
        },
        //headerShown: false,

        // headerLeft: (
        //   <Icon
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //     style={{
        //       paddingLeft: 10
        //     }}
        //   />
        // )
      };
    }
  },
  Checkout: {
    screen: CheckoutTabNavigator,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#5BBC9D"
        },
        headerTitle: "Checkout",
        headerLeft: (
          <Icon
            onPress={() => navigation.openDrawer()}
            name="md-menu"
            size={30}
            style={{
              paddingLeft: 10
            }}
          />
        )
      };
    }
  },
  Detail: {
    screen: Detail,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#5BBC9D"
        },
        headerTitle: navigation.state.params.detailName,
        headerLeft: null,
        headerRight: (
          <Icon
            onPress={() => navigation.navigate("Category")}
            name="ios-close"
            color="white"
            size={50}
            style={{
              paddingRight: 10
            }}
          />
        ),
        gesturesEnabled: false
      };
    }
  },
  TermsAndConditions: {
    screen: TermsAndConditions,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#5BBC9D"
        },
        headerTitle: "Terms & Conditions",
        headerLeft: null,
        headerRight: (
          <Icon
            onPress={() => navigation.navigate("CreditCard")}
            name="ios-close"
            color="white"
            size={50}
            style={{
              paddingRight: 10
            }}
          />
        ),
        gesturesEnabled: false
      };
    }
  },
  EditBasket: {
    screen: EditBasket,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitleStyle: {
          color: "white"
        },
        headerStyle: {
          backgroundColor: "#5BBC9D"
        },
        headerTitle: "Edit Basket Item",
        headerLeft: null,
        headerRight: (
          <Icon
            onPress={() => navigation.navigate("Basket")}
            name="ios-checkmark"
            color="white"
            size={50}
            style={{
              paddingRight: 10
            }}
          />
        ),
        gesturesEnabled: false
      };
    }
  }
});

const HomeDrawNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeStackNavigator
    }
  },
  {
    drawerWidth: Dimensions.get("window").width,
    contentComponent: CustomDrawerComponent
  }
);

const AppSwitchNavigator = createSwitchNavigator({
  welcome:{
    screen: welcome
    
  },
  Login: {
    screen: Login
  },
  Register: {
    screen: Register
    
  },
  Forgot: {
    screen: Forgot
  },
  Settings: {
    screen: Settings
  },
  Home: {
    screen: HomeDrawNavigator
  }
  
});

const AppContainer = createAppContainer(AppSwitchNavigator);


